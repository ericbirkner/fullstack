import React, { Component } from 'react';
import {Link} from 'react-router';

function NoticiaRow(props) {

  console.log(props);
  let retorno = null;
  const getHora = (fecha)=> {
        // Months array
      var months_arr = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

      // Convert timestamp to milliseconds
      var date = new Date(fecha*1000);

      // Year
      var year = date.getFullYear();

      // Month
      var month = months_arr[date.getMonth()];

      // Day
      var day = date.getDate();

      // Hours
      var hours = date.getHours();

      // Minutes
      var minutes = "0" + date.getMinutes();

      // Seconds
      var seconds = "0" + date.getSeconds();

      // Display date time in MM-dd-yyyy h:m:s format
      var convdataTime = month+'-'+day+'-'+year+' '+hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);

      return convdataTime;
  }

    return(
    <li className="row">
        <div className="left">
          <a href={props.data.story_url ? props.data.story_url : props.data.url} target="_blank">
          {props.data.story_title ? props.data.story_title : props.data.title}</a> <span className=" autor">- {props.data.author} -</span>
        </div>
        <div className="right">
        {getHora(props.data.created_at_i)}
          <span className="trash" onClick={()=>props.handleClick(props.data.objectID,props.index)}>&#128465;</span>
        </div>
    </li>
    )



}

export default NoticiaRow;
//if((props.data.title!=null)||(props.data.story_title!=null)){
