import React from 'react';
import { Header } from './Header';
import NoticiaRow from './NoticiaRow';
import axios from 'axios';

export class Root extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      noticias: [],
      borrados:[],
      lista:[]
    }
    this.handleClick = this.handleClick.bind(this);
    this.getData = this.getData.bind(this);
  }


  handleClick(id,index) {
    const noticias = this.state.noticias;
    noticias.splice(index, 1);
    this.setState({ noticias });
    console.log(id);
    axios.post('http://localhost:3000/api/borrados', {'id': id})
    .then(function(res) {
      if(res.status==201) {
        console.log(res);
      }
    })
    .catch(function(err) {
      console.log(err);
    })
    .then(function() {

    });


  }

  getData(){
    axios.get('http://localhost:3000/api/items')
        .then(res => {
          const noticias = res.data;
          this.setState({ noticias });
    })

  }

  componentDidMount() {

    this.getData();

  }

  render() {
    return (
        <div>
        <Header/>
        <div className="container">
            <div className="row">
                <div className="col">
                  <ul id="noticias">
                      { this.state.noticias.map((noticia,index) => <NoticiaRow data={noticia} handleClick={this.handleClick} index={index}></NoticiaRow>)}
                  </ul>

                </div>
            </div>
        </div>
        </div>
    );
  }
}
