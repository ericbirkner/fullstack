import React from 'react';
import {render} from 'react-dom';
import {Router, Route, browserHistory} from 'react-router';

import {Root} from './components/Root';


class App extends React.Component {
  render() {
    return (
        <Router history={browserHistory}>
            <Route path={'/'} component={Root}/>

        </Router>
    );
  }
}

render(
    <App/>, window.document.getElementById('app'));
