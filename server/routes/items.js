'use strict';

var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Item = require('../models/Item.model');
var Borrados = require('../models/Borrados.model');

mongoose.connect('mongodb://mongodb:27017');

router.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


var _borrados=[];

// Get all items
router.get('/items', function(req, res, next) {

  Borrados.find({ "objectID": { $ne: null } })
    .then(function(data) {
      console.log(data);
      data.forEach(function(doc){
        if(doc.objectID){
          console.log('doc'+doc.objectID);
          _borrados.push(doc.objectID)
        }
      }
      );
      Item.find({"objectID":{$nin:_borrados}}).sort({created_at_i:-1}).then(function(data) {
          res.json(data);
      })
  }

  );

  /*
  Item.find().sort({created_at_i:-1})
    .then(function(data) {
      res.json(data);
  });
  */

});

// Get single item
router.get('/items/:id', function(req, res, next) {
  var _id = req.params.id;
  Item.findById(_id, function(err, data) {
    if (err) {
      res.status(404).send();
    } else {
      res.json(data);
    }
  });
});

// Add new item
router.post('/items', function(req, res, next) {
  req.accepts('application/json');
  var item = {
    name: req.body.name,
    category: req.body.category,
    count: req.body.count
  };

  var data = new Item(item);
  data.save(function(err) {
    if (err) {
      res.status(500).send();
    } else {
      res.status(201).send(data._id);
    }
  });
});

// Delete item
router.delete('/items/:id', function(req, res, next) {
  var _id = req.params.id;
  Item.findByIdAndRemove(_id, function(err, data) {
    if (err) {
      res.status(404).send();
    } else {
      res.status(204).send();
    }
  });
});

// Update item
router.put('/items/:id', function(req, res, next) {
  req.accepts('application/json');
  var _id = req.params.id;
  Item.findById(_id, function(err, data) {
    if (err) {
      res.status(404).send();
    } else {
      data.name = req.body.name;
      data.category = req.body.category;
      data.count = req.body.count;
      data.save();
      res.status(200).json(data);
    }
  });
});

//save borrado
router.post('/borrados', function(req, res, next) {
  req.accepts('application/json');
  console.log(req.body.id)
  var item = {
    objectID: req.body.id
  };

  var data = new Borrados(item);
  data.save(function(err) {
    if (err) {
      res.status(500).send();
    } else {
      res.status(201).send(data._id);
    }
  });
});

//list Borrados
// Get all items
router.get('/borrados', function(req, res, next) {
  Borrados.find({ objectID: { $ne: null } })
    .then(function(data) {
      res.json(data);
    });
});


module.exports = router;
