var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ItemSchema = new Schema({
    title: {type: String, required: false},
    story_id: {type: String, required: false},
    story_title: {type: String, required: false},
    story_url: {type: String, required: false},
    url: {type: String, required: false},
    created_at_i: {type: String, required: false},
    comment_text:{type: String, required: false},
    author:{type: String, required: false},
    objectID:{type: String, required: false},
    },
    {versionKey: false}
);

module.exports = mongoose.model('Item', ItemSchema);
