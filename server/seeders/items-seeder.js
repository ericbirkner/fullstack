var mongoose = require('mongoose');
var https = require('https');
var Item = require('../models/Item.model');
var noticias = null;
mongoose.connect('mongodb://mongodb:27017');
Item.collection.drop();

https.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs', function (res) {
    var json = '';
    res.on('data', function (chunk) {
        json += chunk;
    });
    res.on('end', function () {
        if (res.statusCode === 200) {
            try {
                var data = JSON.parse(json);
                // data is available here:
                noticias = data.hits;
                //console.log(noticias);
                poblacion(noticias);

            } catch (e) {
                console.log('Error parsing JSON!');
            }
        } else {
            console.log('Status:', res.statusCode);
        }
    });
}).on('error', function (err) {
      console.log('Error:', err);
});


function poblacion(data){
  console.log(data);
  var count = 0;
  data.forEach((elem) => {

    var noticia = {
      title: elem.title,
      story_id: elem.story_id,
      story_title: elem.story_title,
      story_url: elem.story_url,
      url: elem.url,
      created_at_i: elem.created_at_i,
      comment_text: elem.comment_text,
      author: elem.author,
      objectID:elem.objectID,
    };

    var guarda = new Item(noticia);
    guarda.save(function(err) {
      if (err) {
        console.log(err);
      } else {
        console.log(elem.story_id);
        count++;
        if (count == data.length) quit(count);
      }
    });

  });
}

function quit(total) {
  console.log('Added ',total,' Item seeds');
  mongoose.disconnect();
}
