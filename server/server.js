'use strict';

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
const PORT = process.env.PORT || 3000;
var winston = require('winston');
var logger = require('morgan');
var items = require('./routes/items');
var https = require('https');
var mongoose = require('mongoose');
var Item = require('./models/Item.model');
mongoose.connect('mongodb://mongodb:27017');


//seteo el la carga de data cada una hora
setInterval(function(){
  //console.log('holi');
  getdata();
}, 3600000);

function getdata(){

  https.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs', function (res) {
      var json = '';
      res.on('data', function (chunk) {
          json += chunk;
      });
      res.on('end', function () {
          if (res.statusCode === 200) {
              try {
                  //vacio la tabla para no tener noticias repetidas
                  Item.collection.drop();
                  var count = 0;
                  var data = JSON.parse(json);
                  // data is available here:
                  data.hits.forEach((elem) => {

                    var noticia = {
                      title: elem.title,
                      story_id: elem.story_id,
                      story_title: elem.story_title,
                      story_url: elem.story_url,
                      url: elem.url,
                      created_at_i: elem.created_at_i,
                      comment_text: elem.comment_text,
                      author: elem.author,
                      objectID:elem.objectID,
                    };

                    var guarda = new Item(noticia);
                    guarda.save(function(err) {
                      if (err) {
                        console.log(err);
                      } else {
                        console.log(elem.story_id);
                        count++;
                        if (count == data.length) console.log('end data process');
                      }
                    });

                  });
              } catch (e) {
                  console.log('Error parsing JSON!');
              }
          } else {
              console.log('Status:', res.statusCode);
          }
      });
  }).on('error', function (err) {
        console.log('Error:', err);
  });
}

//se ejecuta una vez para que cargue los datos del Api
getdata();


// Api
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use('/api', items);

app.listen(3000, function() {
  winston.log('info', `Server is listening on port ${PORT}`);
});
