# Test Eric Birkner


## se necesita
- Docker
- Node JS


Instrucciones
---------------
```
clonar el repo git@gitlab.com:ericbirkner/fullstack.git
# install npm dependencies
$ cd server
$ npm install
$ cd ..
$ cd frontend
$ npm install
$ cd ..
$ docker-compose up

ejecutar
```
# run your app (you can stop it with CTRL+C)
$ docker-compose up

# kill containers (DB data will be lost)
$ docker-compose down
```
